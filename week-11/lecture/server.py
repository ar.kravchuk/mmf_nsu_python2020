import pandas as pd
from flask import Flask, request, render_template

app = Flask(__name__)


@app.route('/hello')
def hello_world():
    return '<b> Hello world! </b>'


def read_table():
    if getattr(read_table, '__df', None) is None:
        read_table.__df = pd.read_csv('db.csv')
    return read_table.__df


@app.route('/names')
def names():
    df = read_table()
    return {
        'names': [name for name in df['name']],
    }


@app.route('/mean')
def mean():
    df = read_table()
    return {
        'score': df['score'].mean(),
    }


@app.route('/info/<id_>')
def info(id_):
    df = read_table()
    info_row = df[df['id'] == int(id_)].iloc[0]
    return {
        'name': info_row['name'],
        'score': int(info_row['score']),
    }


@app.route('/info')
def another_info():
    id_ = request.args.get('id')
    df = read_table()
    info_row = df[df['id'] == int(id_)].iloc[0]
    return {
        'name': info_row['name'],
        'score': int(info_row['score']),
    }


@app.route('/get_dict')
def get_dict():
    dct = request.json
    return dct


@app.route('/table')
def render_all():
    df = read_table()
    return render_template('table.html', columns=df.columns, rows=df.values)


if __name__ == '__main__':
    app.run('127.0.0.1', port=1337)
