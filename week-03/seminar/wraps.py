def wraps(wrapped):
    def decorator(wrapper):
        for attr in ('__name__', '__doc__', '__module__'):
            setattr(wrapper, attr, getattr(wrapped, attr))
        return wrapper

    return decorator


def trace(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        """I'm wrapper!"""
        result = f(*args, **kwargs)
        print(f'{f.__name__}, args: {args}, kwargs: {kwargs}, result: {result}')
        return result

    return wrapper


@trace
def g(x):
    """I'm f function!"""
    return x


if __name__ == '__main__':
    g(1)
    print(f'name: {g.__name__}')
    print(f'docstring: {g.__doc__}')
