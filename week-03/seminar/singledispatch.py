from functools import wraps


def singledispatch(f):
    _cache = {}

    def register(g):
        type_arg = list(g.__annotations__.values())[0]
        _cache[type_arg] = g

    @wraps(f)
    def wrapper(arg):
        for k, v in _cache.items():
            if isinstance(arg, k):
                return v(arg)
        return f(arg)

    wrapper.register = register
    return wrapper


@singledispatch
def func(x):
    print('im not implemented')


@func.register
def _(x: int):
    print(x, 'im in value')


@func.register
def _(x: str):
    print(x, 'im string')


if __name__ == '__main__':
    func(1)
    func('some string')
